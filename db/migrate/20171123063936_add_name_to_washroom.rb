class AddNameToWashroom < ActiveRecord::Migration[5.1]
  def change
    add_column :washrooms, :name, :string
    add_column :washrooms, :add_grid_to_washroom, :string
    add_column :washrooms, :grid, :text
    add_column :washrooms, :amenities, :text
  end
end

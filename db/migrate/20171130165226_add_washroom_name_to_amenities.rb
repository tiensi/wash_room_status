class AddWashroomNameToAmenities < ActiveRecord::Migration[5.1]
  def change
    add_column :amenities, :washroom_name, :string
  end
end

class AddIndexToWashroomName < ActiveRecord::Migration[5.1]
  def change
    add_index :amenities, :washroom_name
  end
end

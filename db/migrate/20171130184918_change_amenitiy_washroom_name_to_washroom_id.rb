class ChangeAmenitiyWashroomNameToWashroomId < ActiveRecord::Migration[5.1]
  def change
    remove_column :amenities, :washroom_name, :string
    add_column :amenities, :washroom_id, :integer
  end
end

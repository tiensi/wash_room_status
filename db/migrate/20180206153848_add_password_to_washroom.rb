class AddPasswordToWashroom < ActiveRecord::Migration[5.1]
  def change
    add_column :washrooms, :password, :string
  end
end

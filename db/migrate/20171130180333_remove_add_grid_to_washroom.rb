class RemoveAddGridToWashroom < ActiveRecord::Migration[5.1]
  def change
    remove_column :washrooms, :add_grid_to_washroom
  end
end

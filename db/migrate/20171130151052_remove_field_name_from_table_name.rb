class RemoveFieldNameFromTableName < ActiveRecord::Migration[5.1]
  def change
    remove_column :washrooms, :amenities
  end
end

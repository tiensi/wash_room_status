Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'room_generator#home'
  get '/create', to: 'room_generator#create'
  post '/update_amenity', to: 'room_generator#update_amenity'
  post '/save_room', to: 'room_generator#save_room'
  get '/washroom/:name', to: 'room_manager#show', as: 'get_washroom'
  put '/washroom/:name', to: 'room_manager#update_amenity'

end

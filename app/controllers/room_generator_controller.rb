class RoomGeneratorController < ApplicationController

  helper_method :get_amenity_color
  before_action :get_washroom

  def get_washroom
    if session[:washroom_id]
      @washroom = Washroom.find(session[:washroom_id])
    else
      @washroom = Washroom.create(grid: [5, 11])
      @washroom.save
      session[:washroom_id] = @washroom.id
    end
  end

  def home
  end

  def create
  end

  def save_room
    Rails.logger.info("PARAMS: #{params.inspect}")
    @name_error = nil
    @password_error = nil
    puts params[:name]
    if params[:name] && (Washroom.find_by_name(params[:name]))
      @name_error = "This name has been taken"
    elsif params[:name].nil? || params[:name].empty?
      @name_error = "Name is invalid"
    end
    puts params[:password]
    if params[:password].nil? || params[:password].empty?
      @password_error = "Invalid password"
    end
    if @name_error || @password_error
      respond_to do |format|
        format.js
      end
      return
    end
    @washroom.name = params[:name]
    @washroom.password = params[:password]
    @washroom.save
    redirect_to get_washroom_path(params[:name])
    #Todo reroute to show screen and delete current cache
  end

  def update_amenity
    if params[:commit] == "Delete"
      @washroom.removeAmenity(params)
      @color = "white"
    elsif params[:commit] == "Submit"
      @washroom.addAmenity(params)
      @color = @washroom.getColorByStatus(params[:status])
    end
    @xCoord = params[:xCoor].to_i
    @yCoord = params[:yCoor].to_i
    # Todo update screen with new data
    respond_to do |format|
      format.js
    end
  end
end

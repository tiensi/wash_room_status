class RoomManagerController < ApplicationController
  protect_from_forgery except: :update_amenity
  @@STATUS_LIST = ["out of service", "vacant", "in use"]

  def show()
    #todo add logic to show that not found and redirect to creation
    get_washroom_by_name(params[:name])
  end

  def update_amenity()
    get_washroom_by_name(params[:name])
    if (params[:password] != @washroom.password)
      render json: { errors: 'Invalid password' }, status: 403
      return
    end
    validStatus = get_valid_status(params[:status])
    if (validStatus)
      @washroom.updateAmenity(params[:amenity_name], params[:status].downcase!)
      return
    else
      render json: { errors: 'Invalid status' }, status: 400
    end
  end

  private

  def get_washroom_by_name(name)
    @washroom = Washroom.find_by_name(name)
  end

  def get_valid_status(status)
    @@STATUS_LIST.include? status.downcase!
  end
end

module ApplicationHelper

  def status_options
    ["None", "Vacant", "In Use", "Out of Service"]
  end
end

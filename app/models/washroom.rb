# == Schema Information
#
# Table name: washrooms
#
#  name                :string(255)
#  grid                :text [array]

class Washroom < ApplicationRecord
  validates :grid, presence: true
  validates :name, length: { maximum: 30 }
  validates :password, length: { maximum: 140 }
  serialize :grid

  before_save {
    if (self.name)
      self.name.downcase!
    end
  }
  has_many :amenities, class_name: "Amenity", dependent: :destroy

  def getAmenityStatus(xCoor, yCoor)
    amenity = amenities.find_by_position([xCoor, yCoor])
    if (amenity && !amenity.status.casecmp?("None"))
      amenity.status.titleize
    else
      ""
    end
  end

  def hasAmenity(xCoor, yCoor)
    !!amenities.find_by_position([xCoor, yCoor])
  end

  def getAmenityName(xCoor, yCoor)
    amenity = amenities.find_by_position([xCoor, yCoor])
    if (amenity)
      amenity.name
    else
      ""
    end
  end

  def addAmenity(params)
    amenity = amenities.find_by_position([params[:xCoor].to_i, params[:yCoor].to_i])
    if (amenity)
      amenity.update(position: [params[:xCoor].to_i, params[:yCoor].to_i], name: params[:name], status: params[:status])
    else
      amenities << Amenity.new(position: [params[:xCoor].to_i, params[:yCoor].to_i], name: params[:name], status: params[:status])
    end
  end

  def removeAmenity(params)
    #Remove key value at amenity hash position
    amenity = amenities.find_by_position([params[:xCoor].to_i, params[:yCoor].to_i])
    if (amenity)
      amenity.destroy
    end
  end

  def updateAmenity(name, status)
    #Update status of amenity,
    amenity = amenities.find_by_name(name)
    amenity.update(status: status)
  end

  def getColorForPosition(position)
    amenity = amenities.find_by_position(position)
    if (amenity)
      getColorByStatus(amenity.status)
    else
      puts "no status"
      "white"
    end
  end

  def getColorByStatus(status)
    if status.casecmp?("None")
      "black"
    elsif status.casecmp?("Vacant")
      "green"
    elsif status.casecmp?("In Use")
      "red"
    elsif status.casecmp?("Out of Service")
      "orange"
    else
      "white"
    end
  end

  def get_amenity_color(position)
    getColorForPosition(position)
  end

end

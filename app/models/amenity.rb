class Amenity < ApplicationRecord
  belongs_to :washroom
  default_scope -> { order(created_at: :desc) }
  validates :position, presence: true
  validates :status, presence: true
  serialize :position
  before_save {
    if (status)
      status.downcase!
    end
  }
end
